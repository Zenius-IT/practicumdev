﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
namespace EntryPoint
{
#if WINDOWS || LINUX
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            var fullscreen = false;
        read_input:
            switch (Microsoft.VisualBasic.Interaction.InputBox("Which assignment shall run next? (1, 2, 3, 4, or q for quit)", "Choose assignment", VirtualCity.GetInitialValue()))
            {
                case "1":
                    using (var game = VirtualCity.RunAssignment1(SortSpecialBuildingsByDistance, fullscreen))
                        game.Run();
                    break;
                case "2":
                    using (var game = VirtualCity.RunAssignment2(FindSpecialBuildingsWithinDistanceFromHouse, fullscreen))
                        game.Run();
                    break;
                case "3":
                    using (var game = VirtualCity.RunAssignment3(FindRoute, fullscreen))
                        game.Run();
                    break;
                case "4":
                    using (var game = VirtualCity.RunAssignment4(FindRoutesToAll, fullscreen))
                        game.Run();
                    break;
                case "q":
                    return;
            }
            goto read_input;
        }
        /**Sort all special buildings by Euclidean distance from a specified house.
          The Euclidean distance formula is:  This function takes as input a house position (Vector2 house) and a list of building positions (
          IEnumerable<Vector2> specialBuildings) and returns a sorted list of building positions according
          to their distance from the house position (IEnumerable<Vector2>). Use the merge sort as sorting
          algorithm. Any implementation not using this technique will not be accepted and evaluated.      */

        private static double EuclideanDistance(Point p1, Point p2)
        {
            //Euclidean distance algorthem toegevoegd om te kijken wat de distance is tussen specialbuilding en huis.
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }
        private static double EuclideanCompare(Point p1, Point p2)
        {
            //Hier kijk ik of het gebouw dichtbij is. Hoe groter het getal hoe dichtbijer een vector bij het huis zit.
            //Tevens roep ik hier een andere methode gelijk aan om de doubles om te zetten in een array van doubles en die weer omzetten
            // in integers voor verdere afhandeling in later stadium.
            return closestSpecialBuilding(100 / (1 + EuclideanDistance(p1, p2)));
        }
        private static int closestSpecialBuilding(double a)
        {
            //Afhandeling van doubles naar integers om hier een mergeSort op uit te voeren. i.p.v. VectorPoints wat een float is
            List<Double> listOfDoubles = new List<double>();
            listOfDoubles.Add(a);
            int result = 0;
            foreach (double value in listOfDoubles)
            {
                try
                {
                    result = Convert.ToInt32(value);
                }
                catch (Exception) { }
            }
            //Geeft het resultaat terug als een Integer.
            return result;
        }

        private static void mergeSort(IList<int> inputlist)
        {
            if (inputlist == null || inputlist.Count == 1) return;

            var middleIndexNumber = inputlist.Count / 2;
            var first = inputlist.Take(middleIndexNumber).ToList();
            var last = inputlist.Skip(middleIndexNumber).ToList();

            mergeSort(first);
            mergeSort(last);

            merge(first, last, inputlist);
        }

        private static void merge(List<int> first, List<int> last, IList<int> input)
        {
            // Bereken de totale count dat is hoeveel iteraties de merge moet worden uitgevoerd.
            var totalCountOps = first.Count + last.Count;

            //Tijdelijke optel variable
            var lefttemp = 0;
            var righttemp = 0;

            // Zolang de totale count van de eerste en laatste list nog niet bereikt is. total input.count dus
            for (int i = 0; i < totalCountOps; i++)
            {
                // Als left tijdelijke var > de eerste array. In geval van een oneven verdeelde array
                if (lefttemp >= first.Count)
                {
                    // Pak de input aan de hand van de index in de array en stop hierin de laatste list met zijn index
                    input[i] = last[righttemp++];
                }
                    //vice versa
                else if (righttemp >= last.Count)
                {
                    input[i] = first[lefttemp++];
                }

                else // anders checken we of de eerste list count kleiner is dan de rechter zo ja dan vul je de subList van de linkse array
                    // anders pak je de elementen van de rechter array.

                    // dus first {1 2 3 4 5} < is de gegeven index kleiner? dan de gegeven index van de rechter last{6 4 9 7 8 2 1} <- totale count is 7 dan moet
                    // In de dit geval wel dus we voegen aan de first sublist [lefttemp+1] wil zeggen 1 element van de laatste enzovoorts.
                     
                    input[i] = first[lefttemp] < last[righttemp] ? first[lefttemp++] : last[righttemp++];
            }
        }
        
        private static IEnumerable<Vector2> MergeSortSpecialBuildingsByDistance(Vector2 house, IEnumerable<Vector2> specialBuildings)
        {
            //Hier maar ik een variable sp waarin de List vol met specialBuildings zit.
            var sp = specialBuildings;
            //Maak een soort temp array om straks de lijst voor integers om te gaan mergesorten
            var arrSpecialBuildings = new ArrayList();
            //Tijdelijk List met Integers die we weer mee kunnen nemen naar de mergesort
            List<int> tempArr = new List<int>();
            foreach (var item in sp)
            {
                // Hier worden de vectorpoints die het dichtbijzijnst en verste van de house omgezet naar integer en toegevoegd aan mijn
                // tempArray. Die ik dan weer ga gebruiken voor de mergeSort.
                arrSpecialBuildings.Add(EuclideanCompare(house.ToPoint(), item.ToPoint()));
            }
            foreach(var closestitem in arrSpecialBuildings)
            {
                // Aangezien de arrSpecialBuildings vol zit met doubles moet ik deze voor me mergeSort weer converteren naar ints 
                //Zie code hieronder.
                tempArr.Add(Convert.ToInt32(closestitem));
                               
            }
            // De input list wordt hier nu meegegeven aan de mergeSort om te MergeSorten.(DIVIDE AND CONQUER!!)
            

            //Output van de unsorted List
            foreach(var unsorteditem in tempArr)
            {
                Console.WriteLine("Unsorted List of Closest item to House Highest number means closest to Entry Point" + "   " + unsorteditem);
            }
            mergeSort(tempArr);
            foreach (var sorteditem in tempArr)
            {
                Console.WriteLine("Sorted Array List of Closest Item to House Highest number means closest to Entry Point" + "   " + sorteditem);
            }
            return sp;      
        }

          
        private static IEnumerable<Vector2> SortSpecialBuildingsByDistance(Vector2 house, IEnumerable<Vector2> specialBuildings)
        {
            MergeSortSpecialBuildingsByDistance(house, specialBuildings);

          return specialBuildings.OrderBy(v => Vector2.Distance(v, house));        
        }

    private static IEnumerable<IEnumerable<Vector2>> FindSpecialBuildingsWithinDistanceFromHouse(
      IEnumerable<Vector2> specialBuildings, 
      IEnumerable<Tuple<Vector2, float>> housesAndDistances)
    {
      return
          from h in housesAndDistances
          select
            from s in specialBuildings
            where Vector2.Distance(h.Item1, s) <= h.Item2
            select s;
    }

    private static IEnumerable<Tuple<Vector2, Vector2>> FindRoute(Vector2 startingBuilding, 
      Vector2 destinationBuilding, IEnumerable<Tuple<Vector2, Vector2>> roads)
    {
      var startingRoad = roads.Where(x => x.Item1.Equals(startingBuilding)).First();
      List<Tuple<Vector2, Vector2>> fakeBestPath = new List<Tuple<Vector2, Vector2>>() { startingRoad };
      var prevRoad = startingRoad;
      for (int i = 0; i < 30; i++)
      {
        prevRoad = (roads.Where(x => x.Item1.Equals(prevRoad.Item2)).OrderBy(x => Vector2.Distance(x.Item2, destinationBuilding)).First());
        fakeBestPath.Add(prevRoad);
      }
      return fakeBestPath;
    }

    private static IEnumerable<IEnumerable<Tuple<Vector2, Vector2>>> FindRoutesToAll(Vector2 startingBuilding, 
      IEnumerable<Vector2> destinationBuildings, IEnumerable<Tuple<Vector2, Vector2>> roads)
    {
      List<List<Tuple<Vector2, Vector2>>> result = new List<List<Tuple<Vector2, Vector2>>>();
      foreach (var d in destinationBuildings)
      {
        var startingRoad = roads.Where(x => x.Item1.Equals(startingBuilding)).First();
        List<Tuple<Vector2, Vector2>> fakeBestPath = new List<Tuple<Vector2, Vector2>>() { startingRoad };
        var prevRoad = startingRoad;
        for (int i = 0; i < 30; i++)
        {
          prevRoad = (roads.Where(x => x.Item1.Equals(prevRoad.Item2)).OrderBy(x => Vector2.Distance(x.Item2, d)).First());
          fakeBestPath.Add(prevRoad);
        }
        result.Add(fakeBestPath);
      }
      return result;
    }
  }
#endif
}
